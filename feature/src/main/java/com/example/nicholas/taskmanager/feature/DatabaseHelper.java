package com.example.nicholas.taskmanager.feature;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper
{

    private static final String DB_NAME = "task_manager_database";
    private static final int DB_VERSION = 1;

    public DatabaseHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sqlTasks = "CREATE TABLE tasks(id INTEGER PRIMARY KEY AUTOINCREMENT, name VARCHAR, description VARCHAR)";
        String sqlSubTasks = "CREATE TABLE sub_tasks(id INTEGER PRIMARY KEY AUTOINCREMENT, name VARCHAR, description VARCHAR, task_id INTEGER, FOREIGN KEY(task_id) REFERENCES tasks(id))";
        db.execSQL(sqlTasks);
        db.execSQL(sqlSubTasks);
    }

    public boolean addTasks(String name, String Description) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("name", name);
        contentValues.put("description", Description);
        db.insert("tasks", null, contentValues);
        db.close();
        return true;
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String sqlTasks = "DROP TABLE IF EXISTS tasks";
        String sqlSubTasks = "DROP TABLE IF EXISTS sub_tasks";

        db.execSQL(sqlTasks);
        db.execSQL(sqlSubTasks);

        onCreate(db);
    }
}
