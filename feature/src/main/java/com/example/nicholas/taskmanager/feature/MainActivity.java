package com.example.nicholas.taskmanager.feature;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {


    DatabaseHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        db = new DatabaseHelper(this);

        db.addTasks("Wash Clothes", "You need to go wash your clothes so it can dry tomorrow morning");
        db.addTasks("Buy Toilet Paper", "You need to go buy toilet paper!!");

        Toast.makeText(this, "Values Saved", Toast.LENGTH_LONG).show();
    }
}
